"""
@File    : demo.py
@Time    : 2019/12/10 15:30
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
# 测试用，无意义
import itertools
import time


def pi(N):
    """
    计算pi的值
    # step 1: 创建一个奇数序列: 1, 3, 5, 7, 9, ...
    # step 2: 取该序列的前N项: 1, 3, 5, 7, 9, ..., 2*N-1.
    # step 3: 添加正负符号并用4除: 4/1, -4/3, 4/5, -4/7, 4/9, ...
    # step 4: 求和:
    """
    start_time = time.time()
    total = 0
    data = itertools.count(1, 2)
    for i in enumerate(data):
        if i[1] > 2 * N - 1:
            break
        else:
            if i[0] % 2 == 0:
                total += (4 / i[1])
            else:
                total += (-4 / i[1])
    end_time = time.time()
    return total, end_time - start_time


if __name__ == '__main__':
    # i = (1, 3)
    # res = i[0] % 2 == 0
    # print(res)
    print(pi(10)[0],pi(10)[1])
    print(pi(100)[0],pi(100)[1])
    print(pi(1000)[0],pi(1000)[1])
    print(pi(10000)[0],pi(10000)[1])
    print(pi(100000)[0],pi(100000)[1])
    print(pi(1000000)[0],pi(1000000)[1])
    print(pi(10000000)[0],pi(10000000)[1])
    print('ok')
# 3.1415926
# 3.14159265
# 3.141592654
