"""
@File    : __init__.py.py
@Time    : 2019/11/26 16:10
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


# =====================================================================
#                              接口-公共层                              #
# =====================================================================


class Info:
    __version__ = 'V1.1'
    __author__ = 'LiuFeiYu'

    def __str__(self):
        info_str = 'INFO'
        help_str = 'HELP'
        return f'{info_str.center(40, "*")}\n' \
               f'编写:{self.__author__}\n' \
               f'版本:{self.__version__}\n' \
               f'{help_str.center(40, "*")}\n' \
               f'启动例子:python runMain.py 1\n' \
               f'参数说明:0代表测试环境，1代表线上环境'
