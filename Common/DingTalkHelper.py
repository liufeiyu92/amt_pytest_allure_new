"""
@File    : DingTalkHelper.py
@Time    : 2019/12/17 15:00
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from typing import Union

from dingtalkchatbot.chatbot import DingtalkChatbot
from Common.LogHelper import get_current_time

# 具体使用可参考
# https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq
# https://blog.csdn.net/zhuifengshenku/article/details/79147479
# 钉钉群自定义机器人的token验证码
_access_token = "填你们的钉钉机器人群识别码"
_web_hook = f"https://oapi.dingtalk.com/robot/send?access_token={_access_token}"


class DingTalkSendMsg:
    """自动发送钉钉群发消息"""

    def __init__(self):
        self.ding_talk = DingtalkChatbot(_web_hook)

    def send_msg_by_text(self):
        """text类型消息模板"""
        self.ding_talk.send_text(msg='接口:http://192.168.10.45:8080/job/hy-all-test/48/allure/',
                                 is_at_all=True)

    def send_msg_by_link(self, title='接口自动化测试', text='测试结果请点击'):
        """link类型消息模板"""
        self.ding_talk.send_link(title=title, text=text,
                                 message_url='http://192.168.10.45:8080/job/hy-all-test/48/allure/')

    def send_msg_by_markdown(self, true_num, err_num,
                             allure_report_url="http://192.168.10.45:8080/job/hy-all-test/48/allure"):
        """markdown类型消息模板"""
        _send_time = get_current_time()
        self.ding_talk.send_markdown(title='接口测试报告',
                                     text='### 接口测试\n'
                                          f'> 模块:全部 正确:{true_num} 错误:{err_num}\n\n'
                                          f'> ###### {_send_time}发布 [测试报告-点击查看]({allure_report_url})\n')


if __name__ == '__main__':
    a = DingTalkSendMsg()
    # a.send_msg_by_link()
    # a.send_msg_by_text()
    pass
