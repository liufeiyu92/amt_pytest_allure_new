"""
@File    : TimeHelper.py
@Time    : 2019/12/2 16:46
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
import time
import datetime


class ChangeTime:
    # 1.把datetime转成字符串
    @staticmethod
    def datetime_to_string(datetime_data: datetime):
        return datetime_data.strftime("%Y-%m-%d %H:%M:%S")

    # 2.把字符串转成datetime
    @staticmethod
    def string_to_datetime(string: str):
        return datetime.datetime.strptime(string, "%Y-%m-%d %H:%M:%S")

    # 3.把字符串转成时间戳形式
    @staticmethod
    def string_to_timestamp(string: str):
        return time.mktime(time.strptime(string, "%Y-%m-%d %H:%M:%S"))

    # 4.把时间戳转成字符串形式
    @staticmethod
    def timestamp_to_string(time_stamp):
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time_stamp))

    @staticmethod
    def timestamp_to_string_report(time_stamp):
        return time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(time_stamp))

    # 5.把datetime类型转外时间戳形式
    @staticmethod
    def datetime_to_timestamp(datetime_data: datetime):
        return time.mktime(datetime_data.timetuple())


if __name__ == '__main__':
    # 日期时间字符串
    st = "2017-11-23 16:10:10"
    # 当前日期时间
    dt = datetime.datetime.now()
    # 当前时间戳
    sp = time.time()
    # 1.把datetime转成字符串
    print(ChangeTime.datetime_to_string(dt))
    # 2.把字符串转成datetime
    print(ChangeTime.string_to_datetime(st))
    # 3.把字符串转成时间戳形式
    print(ChangeTime.string_to_timestamp(st))
    # 4.把时间戳转成字符串形式
    print(ChangeTime.timestamp_to_string(sp))
    # 5.把datetime类型转外时间戳形式
    print(ChangeTime.datetime_to_timestamp(dt))
