"""
@File    : ConnectMysql.py
@Time    : 2019/12/13 14:31
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
import pandas as pd


def read_sql(sql, db_obj):
    """
    读取sql文件
    :param sql:sql语句
    :param db_obj: db对象
    :return:
    """
    res = pd.read_sql(sql, db_obj)
    return res.values.tolist()


if __name__ == '__main__':
    # api_complaint_url = (
    #     'http://192.168.10.200/hy/lysh/hy-lysh-merchant-manage/blob/master/doc/api-merchant-complaint-record.md')
    # print(api_complaint_url)
    pass
