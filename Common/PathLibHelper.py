"""
@File    : PathLibHelper.py
@Time    : 2019/12/16 16:51
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from pathlib import *


class PathHelper:

    @staticmethod
    def get_current_path(path_name=''):
        """
        获取当前文件所在目录路径
        :param path_name: 文件路径
        :return: Path对象
        """
        return Path(path_name).cwd()

    @staticmethod
    def get_parent_path(path_name=''):
        """
        获取文件所在目录路径的上一级目录路径
        :param path_name:文件路径
        :return:Path对象
        """
        return Path(path_name).resolve().parent

    @staticmethod
    def get_root_path(path_name=''):
        """
        获取用例所在根目录
        :param path_name:文件路径
        :return:Path对象
        """
        return Path(path_name).resolve().parent.parent

    @staticmethod
    def get_parents_path(path_name=''):
        """
        返回所有上级目录路径
        :param path_name: 文件路径
        :return:Path对象
        """
        return Path(path_name).parents

    @staticmethod
    def get_file_suffix(path_name=''):
        """
        获取文件后缀
        :param path_name:文件路径
        :return:Path对象
        """
        return Path(path_name).suffix


if __name__ == '__main__':
    p1 = PathHelper.get_current_path()
    p2 = PathHelper.get_parent_path()
    # print(p2, p1)
