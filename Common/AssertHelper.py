"""
@File    : AssertHelper.py
@Time    : 2019/12/27 15:14
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


class AssertHelper:
    """assert断言封装"""

    @staticmethod
    def check_assert(*args):
        """检查多assert条件时方法"""
        if len(args) == 0:
            raise Exception('传入检查参数长度为0')
        return all(args[0])
