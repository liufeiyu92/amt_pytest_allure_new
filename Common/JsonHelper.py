"""
@File    : JsonHelper.py
@Time    : 2019/12/17 16:33
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
import demjson


def format_cn_res(data, indent=3):
    """json数据解析"""
    # return json.dumps(data, indent=indent).encode('utf-8').decode('unicode_escape')
    return demjson.encode(data, indent_amount=indent, compactly=False).encode('utf-8').decode('unicode_escape')
