# 说明
此为新结构,老结构请查看
> 老结构: https://gitee.com/liufeiyu92/PytestApiCode

# 环境
* Python 3.7.4
* Pycharm 2019.2.3
* JDK 1.8.0_221

# 运行方法
> 在根目录用命令行启动,示例:python runMain.py -help(查看帮助)

# 目录结构
>Common (封装公共方法)

>Constant (常量)

>Controller (控制器)

>Data (测试数据)

>Decorator (装饰器)

>Log (日志)

>ProjectEnum (封装枚举类)

>Reports (测试报告)

>Script (常量对象层)

>Settings (Config配置模块)

>TestCase (测试用例)

>pytest.ini (配置文件)

>runMain.py (程序主入口)