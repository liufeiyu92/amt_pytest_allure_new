"""
@File    : DatabaseScript.py
@Time    : 2019/12/2 10:54
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *

# =====================================================================
# 测试环境数据库1
test_db_connect_url = DatabaseConstant.DatabaseTest.connect_url
test_db_username = DatabaseConstant.DatabaseTest.username
test_db_password = DatabaseConstant.DatabaseTest.password
test_db_lydd_database = DatabaseConstant.DatabaseTest.lydd
test_db_lyjc_database = DatabaseConstant.DatabaseTest.lyjc
test_db_lysh_database = DatabaseConstant.DatabaseTest.lysh
test_db_lytc_database = DatabaseConstant.DatabaseTest.lytc
test_db_jtgl_database = DatabaseConstant.DatabaseTest.jtgl
test_db_xxfb_database = DatabaseConstant.DatabaseTest.xxfb
# =====================================================================
# 正式环境数据库
online_db_connect_url = DatabaseConstant.DatabaseProduction.connect_url
online_db_username = DatabaseConstant.DatabaseProduction.username
online_db_password = DatabaseConstant.DatabaseProduction.password
online_db_lydd_database = DatabaseConstant.DatabaseProduction.lydd
online_db_lyjc_database = DatabaseConstant.DatabaseProduction.lyjc
online_db_lysh_database = DatabaseConstant.DatabaseProduction.lysh
online_db_lytc_database = DatabaseConstant.DatabaseProduction.lytc
online_db_jtgl_database = DatabaseConstant.DatabaseProduction.jtgl
online_db_xxfb_database = DatabaseConstant.DatabaseProduction.xxfb
# =====================================================================
