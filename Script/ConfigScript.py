"""
@File    : ConfigScript.py
@Time    : 2019/12/18 10:26
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *

# debug
title_debug = ConfigConstant.ConfigTitle.TITLE_DEBUG
# jenkins
title_jenkins = ConfigConstant.ConfigTitle.TITLE_JENKINS
# email
title_email = ConfigConstant.ConfigTitle.TITLE_EMAIL
# jira
title_jira = ConfigConstant.ConfigTitle.TITLE_JIRA
