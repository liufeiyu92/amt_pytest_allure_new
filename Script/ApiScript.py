"""
@File    : ApiScript.py
@Time    : 2019/12/10 11:41
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *


class LyjcScript:
    """行业监测"""
    # # debug-tourism-resource-lyjc
    lyjc_tourism_res_add_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_ADD)
    lyjc_tourism_res_update_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_UPDATE)
    lyjc_tourism_res_select_id_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_SELECT_ID)
    lyjc_tourism_res_delete_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_DELETE)
    lyjc_tourism_res_select_list_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_SELECT_LIST)
    lyjc_tourism_res_area_list_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_AREA_LIST)
    lyjc_tourism_res_type_list_debug = api_url.get(LYJC.VALUE_LYJC_TOURISM_RES_TYPE_LIST)


if __name__ == '__main__':
    # print(LyjcScript.lyjc_tourism_res_add_debug)
    pass
