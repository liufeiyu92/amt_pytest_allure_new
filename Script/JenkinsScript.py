"""
@File    : JenkinsScript.py
@Time    : 2019/12/2 15:40
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *

# 连接地址
jenkins_connect_url = JenkinsConstant.Jenkins.connect_url
# 用户名
jenkins_username = JenkinsConstant.Jenkins.username
# 密码
jenkins_password = JenkinsConstant.Jenkins.password
# api密码
jenkins_api_password = JenkinsConstant.Jenkins.api_password
# allure报告地址
jenkins_allure_report_url = JenkinsConstant.Jenkins().allure_report_url
