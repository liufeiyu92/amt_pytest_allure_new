"""
@File    : HostScript.py
@Time    : 2019/12/2 14:44
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *
from Script import ConfigScript

# =====================================================================
# 测试环境Host

# 交通管理
jtgl_test_host = HostConstant.HostTest.jtgl
# 旅游商户
lysh_test_host = HostConstant.HostTest.lysh
# 信息发布
xxfb_test_host = HostConstant.HostTest.xxfb
# 旅游停车
lytc_test_host = HostConstant.HostTest.lytc
# 旅游监测
lyjc_test_host = HostConstant.HostTest.lyjc
# 登录
login_test_host = HostConstant.HostTest.login
# =====================================================================
# 正式环境Host

# 交通管理
jtgl_pro_host = HostConstant.HostProduction.jtgl
# 旅游商户
lysh_pro_host = HostConstant.HostProduction.lysh
# 信息发布
xxfb_pro_host = HostConstant.HostProduction.xxfb
# 旅游停车
lytc_pro_host = HostConstant.HostProduction.lytc
# 旅游监测
lyjc_pro_host = HostConstant.HostProduction.lyjc
# 登录
login_pro_host = HostConstant.HostProduction.login
# =====================================================================
# 调用环境Host

# 交通管理
jtgl_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.LYJT_HOST.value))
# 旅游商户
lysh_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.LYSH_HOST.value))
# 信息发布
xxfb_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.XXFB_HOST.value))
# 旅游停车
lytc_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.LYTC_HOST.value))
# 旅游监测
lyjc_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.LYJC_HOST.value))
# 登录
login_main_host = Config.get_config((ConfigScript.title_debug, HostEnum.HostMain.LOGIN_HOST.value))
# =====================================================================
