"""
@File    : JiraScript.py
@Time    : 2019/12/2 16:25
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script import *

# 连接地址
jira_connect_url = JiraConstant.Jira.connect_url

# 用户名
jira_username = JiraConstant.Jira.username

# 密码
jira_password = JiraConstant.Jira.password

# 开关
jira_switch = JiraConstant.Jira().switch
