"""
@File    : BaseController.py
@Time    : 2019/11/26 16:11
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from abc import ABCMeta, abstractmethod


class Controller(metaclass=ABCMeta):
    """配置-控制层-基类"""

    @abstractmethod
    def init_config(self):
        return
