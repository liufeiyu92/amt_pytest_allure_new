"""
@File    : testController.py
@Time    : 2019/11/26 16:32
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Script.HostScript import *
from Settings.Config import Config
from Script.ConfigScript import title_debug, HostEnum
from Controller.BaseController import Controller


class TestController(Controller):
    def init_config(self):  # noqa
        old_host_list = [HostEnum.HostMain.LYJC_HOST.value,
                         HostEnum.HostMain.LYJT_HOST.value,
                         HostEnum.HostMain.LOGIN_HOST.value,
                         HostEnum.HostMain.LYSH_HOST.value,
                         HostEnum.HostMain.LYTC_HOST.value,
                         HostEnum.HostMain.XXFB_HOST.value]
        new_host_list = [lyjc_test_host,
                         jtgl_test_host,
                         login_test_host,
                         lysh_test_host,
                         lytc_test_host,
                         xxfb_test_host]
        data = list(zip(old_host_list, new_host_list))
        for i in data:
            Config.set_config(title=title_debug,
                              old=i[0],
                              new=i[1])


if __name__ == '__main__':
    TestController().init_config()
