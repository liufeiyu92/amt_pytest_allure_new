"""
@File    : __init__.py.py
@Time    : 2019/11/26 16:10
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
import os
from configparser import ConfigParser

# =====================================================================
#                              接口-配置层                              #
# =====================================================================

config = ConfigParser()
pytest_config = ConfigParser()

# Config.ini路径
conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'Config.ini')
# pytest.ini路径
pytest_config_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'pytest.ini')
# 读取Config.ini
config.read(conf_path, encoding='utf-8')
# 读取pytest.ini
pytest_config.read(pytest_config_path, encoding='utf-8')

# 判断配置文件是否存在
if not os.path.exists(conf_path) or not os.path.exists(pytest_config_path):
    raise FileNotFoundError(f"请确保配置文件存在！config.ini路径:{conf_path} pytest.ini路径:{pytest_config_path}")
