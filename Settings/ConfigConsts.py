"""
@File    : ConfigConsts.py
@Time    : 2019/12/2 10:50
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
# ==========================================================================
# 此py文件无意义

# ==========================================================================
# [mail]
VALUE_SMTP_SERVER = "smtpserver"
VALUE_SENDER = "sender"
VALUE_RECEIVER = "receiver"
VALUE_USERNAME = "username"
VALUE_PASSWORD = "password"
# ==========================================================================

# ==========================================================================
# jira
VALUE_JIRA_URL = "connect_url"
VALUE_JIRA_USERNAME = "username"
VALUE_JIRA_PASSWORD = "password"
VALUE_JIRA_ISSUE_SWITCH = "switch"

# ==========================================================================
# jenkins
VALUE_JENKINS_URL = "connect_url"
VALUE_JENKINS_USER_NAME = "username"
VALUE_JENKINS_PASSWORD = "password"
VALUE_JENKINS_API_PASSWORD = "api_password"
VALUE_JENKINS_ALLURE_REPORT_URL = "allure_report_url"

# ==========================================================================
# [debug/release]
VALUE_TESTER = "tester"
VALUE_ENVIRONMENT = "environment"
VALUE_VERSION_CODE = "versionCode"
VALUE_JTGL_HOST = "jtgl_host"
VALUE_SH_HOST = "lysh_host"
VALUE_XXFB_HOST = "xxfb_host"
VALUE_LYTC_HOST = "lytc_host"
VALUE_LYJC_HOST = "lyjc_host"
VALUE_LOGIN_HOST = "loginHost"
VALUE_LOGIN_INFO = "loginInfo"
VALUE_TOKEN = "token"

# # ==========================================================================
# # host-debug
# self.tester_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_TESTER)
# self.environment_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_ENVIRONMENT)
# self.versionCode_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_VERSION_CODE)
# self.host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_JTGL_HOST)
# self.merchant_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_SH_HOST)
# self.xxfb_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_XXFB_HOST)
# self.lytc_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LYTC_HOST)
# self.lyjc_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LYJC_HOST)
# self.loginHost_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LOGIN_HOST)
# self.loginInfo_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LOGIN_INFO)
# self.token_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)
#
# # ==========================================================================
# # debug-tourism-resource-lyjc
# self.lyjc_tourism_res_add_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_ADD)
# self.lyjc_tourism_res_update_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_UPDATE)
# self.lyjc_tourism_res_select_id_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_SELECT_ID)
# self.lyjc_tourism_res_delete_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_DELETE)
# self.lyjc_tourism_res_select_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_SELECT_LIST)
# self.lyjc_tourism_res_area_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_AREA_LIST)
# self.lyjc_tourism_res_type_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_TYPE_LIST)
#
# # ==========================================================================
# # debug-parking-lot-lytc
# self.lytc_parking_lot_add_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_ADD)
# self.lytc_parking_lot_delete_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_DELETE)
# self.lytc_parking_lot_update_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_UPDATE)
# self.lytc_parking_lot_select_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_GET)
# self.lytc_parking_lot_select_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_LIST)
# self.lytc_parking_lot_select_top_five_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_TOP_FIVE)
# # debug-parking-realtime-lytc
# self.lytc_parking_realtime_select_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_REALTIME_SELECT_LIST)
# self.lytc_parking_summary_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_SUMMARY)
# self.lytc_parking_select_all_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_SELECT_ALL)
# # debug-scenic-area-prediction-lytc
# self.lytc_scenic_area_prediction_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_SCENIC_AREA_PREDICTION)
#
# # ==========================================================================
# # debug-area-xxfb
# self.xxfb_area_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_ADD)
# self.xxfb_area_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_UPDATE)
# self.xxfb_area_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_DELETE)
# self.xxfb_area_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_SELECT)
# # debug-device-xxfb
# self.xxfb_device_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_ADD)
# self.xxfb_device_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_UPDATE)
# self.xxfb_device_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_DELETE)
# self.xxfb_device_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_SELECT)
# # debug-interface-xxfb
# self.xxfb_interface_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_ADD)
# self.xxfb_interface_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_UPDATE)
# self.xxfb_interface_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_DELETE)
# self.xxfb_interface_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_SELECT)
#
# # ==========================================================================
# # debug-user-jtgl
# self.user_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LIST)
# self.user_company_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_COMPANY_LIST)
# self.user_department_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_DEPARTMENT_LIST)
# self.user_role_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_ROLE_ID)
# self.user_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_ADD)
# self.user_logout_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOGOUT)
# # debug-menu-jtgl
# self.menu_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MENU_SELECT)
# self.menu_select_by_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_SELECT_ID)
# # debug-role-jtgl
# self.role_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROLE_LIST)
# # debug-road-jtgl
# self.road_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_ADD)
# self.road_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_UPDATE)
# self.road_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_DELETE)
# self.road_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_SELECT)
# self.road_relation_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_RELATION)
# self.road_important_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_IMPORTANT)
# # debug-device-jtgl
# self.device_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_DEVICE_SELECT)
# # debug-monitor-jtgl
# self.monitor_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_ADD)
# self.monitor_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_UPDATE)
# self.monitor_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_DELETE)
# self.monitor_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_SELECT)
# self.monitor_relation_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_RELATION)
# self.monitor_home_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_HOME)
# # debug-msg-jtgl
# self.msg_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_ADD)
# self.msg_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_UPDATE)
# self.msg_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_DELETE)
# self.msg_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_SELECT)
# # debug-msg-param-jtgl
# self.msg_param_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_PARAM_UPDATE)
# self.msg_param_get_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_PARAM_GET)
# # debug-alarm-jtgl
# self.alarm_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_ALARM_ADD)
# self.alarm_ignore_debug = _API_URL_DEBUG.get(Config.VALUE_API_ALARM_IGNORE)
# # debug-send-msg-jtgl
# self.send_msg_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG)
# self.send_msg_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_LIST)
# self.send_msg_alarm_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_ALARM)
# self.send_msg_detail_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_DETAIL)
# # debug-industry-jtgl
# self.industry_pro_debug = _API_URL_DEBUG.get(Config.VALUE_API_INDUSTRY_PRO)
# self.industry_city_debug = _API_URL_DEBUG.get(Config.VALUE_API_INDUSTRY_CITY)
# # debug-area-points-jtgl
# self.area_points_debug = _API_URL_DEBUG.get(Config.VALUE_API_AREA_POINTS)
# # debug-screen-list-jtgl
# self.screen_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_SCREEN_LIST)
# # debug-car-source-jtgl
# self.car_source_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_SOURCE_STATISTICS)
# self.car_source_top_ten_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_SOURCE_TOP_TEN)
# # debug-car-flow-jtgl
# self.car_flow_obj_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_OBJ)
# self.car_flow_statistics_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_STATISTICS)
# self.car_flow_real_time_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_REAL_TIME)
# # debug-traffic-statics-jtgl
# self.traffic_statics_report_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_REPORT)
# self.traffic_statics_trend_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_TREND)
# self.traffic_statics_info_sub_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_INFO_SUB)
# self.traffic_statics_road_info_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_ROAD_INFO)
# self.traffic_statics_road_top5_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_ROAD_TOP_FIVE)
# self.traffic_statics_jam_top5_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_JAM_TOP_FIVE)
# self.traffic_statics_jam_ratio_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_JAM_RATIO)
# self.traffic_statics_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_LIST)
# # debug-user-log-jtgl
# self.user_log_page_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_PAGE_SELECT)
# self.user_log_user_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_USER_SELECT)
# self.user_log_model_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_MODEL_SELECT)
#
# # ==========================================================================
# # debug-merchant-lysh
# self.merchant_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_ADD)
# self.merchant_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_UPDATE)
# self.merchant_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_SELECT)
# self.merchant_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_DELETE)
# # debug-integrity-lysh
# self.integrity_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_INTEGRITY_SELECT)
# # debug-complaint-lysh
# self.complaint_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_COMPLAINT_SELECT)
# # debug-inspect-lysh
# self.inspect_report_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_REPORT)
# self.inspect_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_LIST)
# self.inspect_select_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_SELECT_ID)
# self.inspect_warn_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_WARN_LIST)
# self.inspect_rec_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_RECTIFICATION_LIST)
# self.inspect_complete_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_COMPLETE_LIST)
# self.inspect_rectification_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_RECTIFICATION)
#
# # ==========================================================================
# # release
# self.tester_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_TESTER)
# self.environment_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_ENVIRONMENT)
# self.versionCode_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_VERSION_CODE)
# self.host_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_JTGL_HOST)
# self.loginHost_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_LOGIN_HOST)
# self.loginInfo_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_LOGIN_INFO)
#
# # ==========================================================================
# # email
# self.smtpserver = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_SMTP_SERVER)
# self.sender = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_SENDER)
# self.receiver = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_RECEIVER)
# self.username = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_USERNAME)
# self.password = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_PASSWORD)
