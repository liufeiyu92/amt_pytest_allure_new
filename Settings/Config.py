"""
@File    : Config.py
@Time    : 2019/11/26 16:14
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Common.LogHelper import *
from Common.TimeHelper import *
from Settings import *


class Config:
    """配置-数据层"""

    # path
    path_dir = str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

    def __init__(self):
        self.log = MyLog()
        self.xml_report_path = Config.path_dir + '/Reports/xml/' + ChangeTime.timestamp_to_string_report(time.time())
        self.html_report_path = Config.path_dir + '/Reports/html/' + ChangeTime.timestamp_to_string_report(time.time())

    @staticmethod
    def set_config(title, old, new):
        """
        提供设置配置项
        :param title: 标签
        :param old: 要修改的值
        :param new: 新值
        :return:
        """
        config.set(title, old, new)
        with open(conf_path, "w+", encoding="utf-8") as f:
            return config.write(f)

    @staticmethod
    def get_config(parameter: tuple):
        """
        获取配置项
        :param parameter: 参数 -> 元祖类型 -> (title , value)
        :return:配置项
        """
        return config.get(parameter[0], parameter[1])

    @staticmethod
    def get_pytest_cof(parameter: tuple):
        """
        pytest配置获取
        :param parameter: 参数 -> 元祖类型 -> (title , value)
        :return:配置项
        """
        return config.get(parameter[0], parameter[1])

    @staticmethod
    def set_conf(title, value, text):
        """
        配置文件修改
        :param title: 标签
        :param value: 要修改的值
        :param text: 新值
        :return:
        """
        config.set(title, value, text)
        with open(conf_path, "w+", encoding="utf-8") as f:
            return config.write(f)

    @staticmethod
    def add_conf(title):
        """
        配置文件添加
        :param title: 标签
        :return:
        """
        config.add_section(title)
        with open(conf_path, "w+") as f:
            return config.write(f)


if __name__ == '__main__':
    print(Config.get_config(("private_debug", "test_lyjt_host")))
