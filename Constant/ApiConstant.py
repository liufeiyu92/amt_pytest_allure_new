"""
@File    : ApiConstant.py
@Time    : 2019/12/10 11:13
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


class ErrorApiNumber:
    """接口测试结果"""
    # 接口测试结果，错误数量
    API_ERROR_RESPONSE_NUMBER = 0
    # 正确数量
    API_TRUE_RESPONSE_NUMBER = 0


class LYJC:
    """洪雅-行业监测"""
    # 旅游资源模块-行业监测
    VALUE_LYJC_TOURISM_RES_ADD = "api_lyjc_tourism_resource_add"
    VALUE_LYJC_TOURISM_RES_UPDATE = "api_lyjc_tourism_resource_update"
    VALUE_LYJC_TOURISM_RES_SELECT_ID = "api_lyjc_tourism_resource_select_by_id"
    VALUE_LYJC_TOURISM_RES_DELETE = "api_lyjc_tourism_resource_delete_by_id"
    VALUE_LYJC_TOURISM_RES_SELECT_LIST = "api_lyjc_tourism_resource_select_list"
    VALUE_LYJC_TOURISM_RES_AREA_LIST = "api_lyjc_tourism_resource_select_area_list"
    VALUE_LYJC_TOURISM_RES_TYPE_LIST = "api_lyjc_tourism_resource_select_type_list"


class LYTC:
    # 停车场基础信息-停车管理
    VALUE_LYTC_PARKING_LOT_ADD = "api_lytc_parking_lot_add"
    VALUE_LYTC_PARKING_LOT_DELETE = "api_lytc_parking_lot_delete"
    VALUE_LYTC_PARKING_LOT_UPDATE = "api_lytc_parking_lot_update"
    VALUE_LYTC_PARKING_LOT_SELECT_GET = "api_lytc_parking_lot_select"
    VALUE_LYTC_PARKING_LOT_SELECT_LIST = "api_lytc_parking_lot_select_list"
    VALUE_LYTC_PARKING_LOT_SELECT_TOP_FIVE = "api_lytc_parking_lot_select_top_five"
    # 车位饱和度实时信息-停车管理
    VALUE_LYTC_PARKING_REALTIME_SELECT_LIST = "api_lytc_parking_realtime_select_list"
    VALUE_LYTC_PARKING_SUMMARY = "api_lytc_parking_summary"
    VALUE_LYTC_PARKING_SELECT_ALL = "api_lytc_parking_space_select_all"
    # 景区停车预测模块-停车管理
    VALUE_LYTC_SCENIC_AREA_PREDICTION = "api_lytc_scenic_area_parking_prediction"


class XXFB:
    # ==========================================================================
    # 区域管理模块-信息发布
    VALUE_XXFB_AREA_ADD = "api_xxfb_area_add"
    VALUE_XXFB_AREA_UPDATE = "api_xxfb_area_update"
    VALUE_XXFB_AREA_DELETE = "api_xxfb_area_delete"
    VALUE_XXFB_AREA_SELECT = "api_xxfb_area_select"
    # 设备管理模块-信息发布
    VALUE_XXFB_DEVICE_ADD = "api_xxfb_device_add"
    VALUE_XXFB_DEVICE_UPDATE = "api_xxfb_device_update"
    VALUE_XXFB_DEVICE_DELETE = "api_xxfb_device_delete"
    VALUE_XXFB_DEVICE_SELECT = "api_xxfb_device_select"
    # 接口管理模块-信息发布
    VALUE_XXFB_INTERFACE_ADD = "api_xxfb_interface_add"
    VALUE_XXFB_INTERFACE_UPDATE = "api_xxfb_interface_update"
    VALUE_XXFB_INTERFACE_DELETE = "api_xxfb_interface_delete"
    VALUE_XXFB_INTERFACE_SELECT = "api_xxfb_interface_select"


class JTGL:
    # ==========================================================================
    # 用户模块-交通管理
    VALUE_API_USER_LIST = "api_user_list"
    VALUE_API_USER_COMPANY_LIST = "api_user_company_list"
    VALUE_API_USER_DEPARTMENT_LIST = "api_user_department_list"
    VALUE_API_USER_ROLE_ID = "api_user_role_id"
    VALUE_API_USER_ADD = "api_user_add"
    VALUE_API_USER_LOGOUT = "api_user_logout"
    # 菜单模块-交通管理
    VALUE_API_MENU_SELECT = "api_menu_select"
    VALUE_API_SELECT_ID = "api_menu_select_by_id"
    # 角色模块-交通管理
    VALUE_API_ROLE_LIST = "api_role_list"
    # 设备资源-交通管理
    VALUE_API_DEVICE_SELECT = "api_device_select"  # 分页获取设备信息
    # 路段资源-交通管理
    VALUE_API_ROAD_ADD = "api_road_add"  # 新增路段
    VALUE_API_ROAD_UPDATE = "api_road_update"  # 修改路段
    VALUE_API_ROAD_DELETE = "api_road_delete"  # 删除路段
    VALUE_API_ROAD_SELECT = "api_road_select"  # 分页查看路段
    VALUE_API_ROAD_RELATION = "api_road_relation"  # 获取关联对象信息
    VALUE_API_ROAD_IMPORTANT = "api_road_important"  # 设置路段为重点关注路段
    # 监控点信息-交通管理
    VALUE_API_MONITOR_ADD = "api_monitor_add"  # 新增监测点
    VALUE_API_MONITOR_UPDATE = "api_monitor_update"  # 修改监测点
    VALUE_API_MONITOR_DELETE = "api_monitor_delete"  # 删除监测点
    VALUE_API_MONITOR_SELECT = "api_monitor_select"  # 查看监控点
    VALUE_API_MONITOR_RELATION = "api_monitor_relation"  # 获取关联区域
    VALUE_API_MONITOR_HOME = "api_monitor_home"  # 获取首页监控点数据
    # 短信联系人-交通管理
    VALUE_API_MSG_ADD = "api_msg_add"  # 新增短信联系人
    VALUE_API_MSG_UPDATE = "api_msg_update"  # 修改短信联系人
    VALUE_API_MSG_DELETE = "api_msg_delete"  # 删除短信联系人
    VALUE_API_MSG_SELECT = "api_msg_select"  # 查看短信联系人
    # 短信配置-交通管理
    VALUE_API_MSG_PARAM_UPDATE = "api_msg_param_update"  # 修改短信配置
    VALUE_API_MSG_PARAM_GET = "api_msg_param_get"  # 获取电信配置
    # 交通告警-交通管理
    VALUE_API_ALARM_ADD = "api_alarm_add"  # 添加交通告警
    VALUE_API_ALARM_IGNORE = "api_alarm_ignore"  # 忽略交通告警
    # 发送短信-交通管理
    VALUE_API_SEND_MSG = "api_send_msg"  # 添加发送短信
    VALUE_API_SEND_MSG_LIST = "api_send_msg_list"  # 分页获取短信列表
    VALUE_API_SEND_MSG_ALARM = "api_send_msg_list_alarm"  # 获取类型用户列表和短信内容
    VALUE_API_SEND_MSG_DETAIL = "api_send_msg_detail"  # 获取短信详情
    # 产业运行监测-交通管理
    VALUE_API_INDUSTRY_PRO = "api_industry_province"  # 获取所有省份
    VALUE_API_INDUSTRY_CITY = "api_industry_city"  # 获取所有城市
    # 区域坐标点-交通管理
    VALUE_API_AREA_POINTS = "api_area_points"  # 获取区域坐标点
    # 诱导屏管理-交通管理
    VALUE_API_SCREEN_LIST = "api_screen_list"  # 获取诱导屏数据
    # 车来源管理-交通管理
    VALUE_API_CAR_SOURCE_STATISTICS = "api_car_source_statistics"  # 获取车来源统计数据
    VALUE_API_CAR_SOURCE_TOP_TEN = "api_car_source_top_ten"  # 导出车来源TOP10数据
    # 车流量管理-交通管理
    VALUE_API_CAR_FLOW_OBJ = "api_flow_object"  # 获取统计对象
    VALUE_API_CAR_FLOW_STATISTICS = "api_flow_statistics"  # 获取车流量统计数据
    VALUE_API_CAR_FLOW_REAL_TIME = "api_flow_real_time"  # 获取车流量实时数据
    # 交通统计-交通管理
    VALUE_API_TRAFFIC_STATICS_REPORT = "api_traffic_statics_report"  # 获取交通报表统计数据
    VALUE_API_TRAFFIC_STATICS_TREND = "api_traffic_statics_trend"  # 获取交通趋势分析数据
    VALUE_API_TRAFFIC_STATICS_INFO_SUB = "api_traffic_statics_info_sub"  # 旅游干道拥堵指数
    VALUE_API_TRAFFIC_STATICS_ROAD_INFO = "api_traffic_statics_road_info"  # 实时路况详情
    VALUE_API_TRAFFIC_STATICS_ROAD_TOP_FIVE = "api_traffic_statics_road_info_top5"  # 路段实时拥堵指数top5
    VALUE_API_TRAFFIC_STATICS_JAM_TOP_FIVE = "api_traffic_statics_jam_top5"  # 路段当日拥堵时长top5
    VALUE_API_TRAFFIC_STATICS_JAM_RATIO = "api_traffic_statics_jam_ratio"  # 拥堵里程汇总
    VALUE_API_TRAFFIC_STATICS_LIST = "api_traffic_statics_list"  # 获取实时路况详情筛选条件
    # 用户操作记录-交通管理
    VALUE_API_USER_LOG_PAGE_SELECT = "api_user_log_page_select"  # 分页查询用户操作记录
    VALUE_API_USER_LOG_USER_SELECT = "api_user_log_user_select"  # 查询系统所有用户
    VALUE_API_USER_LOG_MODEL_SELECT = "api_user_log_model_select"  # 查询系统所有模块


class LYSH:
    # ==========================================================================
    # 商户模块-商户系统
    VALUE_API_SH_ADD = "api_merchant_info_add"  # 新增商户
    VALUE_API_SH_UPDATE = "api_merchant_info_update"  # 修改商户
    VALUE_API_SH_SELECT = "api_merchant_info_select"  # 查看商户
    VALUE_API_SH_DELETE = "api_merchant_info_delete"  # 删除商户
    # 商户诚信模块-商户系统
    VALUE_API_INTEGRITY_SELECT = "api_integrity_select"  # 查询商户诚信列表
    # 商户投诉服务模块-商户系统
    VALUE_API_COMPLAINT_SELECT = "api_complaint_select"  # 查询商户投诉列表
    # 巡查管理+经营预警-商户系统
    VALUE_API_INSPECT_REPORT = "api_inspect_report"  # 巡检上报
    VALUE_API_INSPECT_LIST = "api_inspect_list"  # 巡查管理列表
    VALUE_API_INSPECT_SELECT_ID = "api_inspect_select_id"  # 根据ID查询巡查
    VALUE_API_INSPECT_WARN_LIST = "api_inspect_warn_list"  # 经营预警列表下发
    VALUE_API_INSPECT_RECTIFICATION_LIST = "api_inspect_rectification_list"  # 经营预警列表整改
    VALUE_API_INSPECT_COMPLETE_LIST = "api_inspect_complete_list"  # 经营预警列表完成
    VALUE_API_INSPECT_RECTIFICATION = "api_inspect_rectification"  # 整改
