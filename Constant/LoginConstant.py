"""
@File    : LoginConstant.py
@Time    : 2019/12/17 16:15
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Constant import *


class Login:
    """登录所需常量"""
    _LOGIN_INFO = 'login_info'
    _VALUE_TOKEN = 'token'

    @property
    def token(self):
        """获取token"""
        para = (ConfigTitle.TITLE_DEBUG, self._VALUE_TOKEN)
        return Config.Config.get_config(para)

    @token.setter
    def token(self, value: str):
        """设置全局变量token"""
        Config.Config.set_config(ConfigTitle.TITLE_DEBUG, self._VALUE_TOKEN, value)

    @property
    def login_info(self):
        """登录所需要的参数"""
        para = (ConfigTitle.TITLE_DEBUG, self._LOGIN_INFO)
        return Config.Config.get_config(para)


if __name__ == '__main__':
    pass
