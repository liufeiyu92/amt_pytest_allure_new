"""
@File    : JiraConstant.py
@Time    : 2019/12/2 16:19
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Constant import *


class Jira:
    """jira常量"""
    # 连接地址
    connect_url = "http://192.168.10.216:8200"

    # 连接用户名
    username = "liufeiyu92"

    # 连接密码
    password = "123456"

    # 开关
    _VALUE_JIRA_ISSUE_SWITCH = "switch"

    @property
    def switch(self):
        """
        获取是否自动提交问题单
        0为关闭,1为开启
        :return:
        """
        para = (ConfigTitle.TITLE_JIRA, self._VALUE_JIRA_ISSUE_SWITCH)
        return Config.Config.get_config(para)

    @switch.setter
    def switch(self, value: str):
        """
        自动提交问题单开关
        :param value: 0为关闭 1为开启
        :return:
        """
        if value != "0" and value != "1":
            raise Exception('请传入正确的参数,0或者1')
        else:
            Config.Config.set_config(ConfigTitle.TITLE_JIRA, self._VALUE_JIRA_ISSUE_SWITCH, value)


if __name__ == '__main__':
    pass
