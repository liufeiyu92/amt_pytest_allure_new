"""
@File    : HostConstant.py
@Time    : 2019/12/2 14:19
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


class HostTest:
    """测试环境连接地址"""
    # 测试环境host地址
    jtgl = "http://test.lyjtgl.hongya.com/lyjt"
    lysh = "http://test.lysh.hongya.com/lysh"
    xxfb = "http://test.lyxxfb.hongya.com/lyxxfb"
    lytc = "http://test.lytc.hongya.com/lytc"
    lyjc = "http://test.lyjc.hongya.com/lyjc"
    login = "http://test.hydd.hongya.com/login"


class HostProduction:
    """正式环境连接地址"""
    # 正式环境host地址
    jtgl = "http://lyjt.hongya.cdsunrise.net:28666/lyjt"
    lysh = "http://lysh.hongya.cdsunrise.net:28666/lysh"
    xxfb = "http://lyfb.hongya.cdsunrise.net:28666/lyxxfb"
    lytc = "http://lytc.hongya.cdsunrise.net:28666/lytc"
    lyjc = "http://lyjc.hongya.cdsunrise.net:28666/lyjc"
    login = "http://hydd.hongya.cdsunrise.net:28666/login"
