"""
@File    : ConfigConstant.py
@Time    : 2019/12/13 14:44
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


class ConfigTitle:
    """配置文件抬头"""
    # titles:
    TITLE_DEBUG = "private_debug"
    TITLE_RELEASE = "online_release"
    TITLE_EMAIL = "mail"
    TITLE_DATABASE_66 = "database66"
    TITLE_DATABASE_181 = "database181"
    TITLE_DATABASE_184 = "database184"
    TITLE_DATABASE_201 = "database201"
    TITLE_JENKINS = "local_jenkins"
    TITLE_JIRA = "company_jira"


class AllureReportUrl:
    """allure报告地址"""
    VALUE_JENKINS_ALLURE_REPORT_URL = "allure_report_url"
