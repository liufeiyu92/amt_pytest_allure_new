"""
@File    : DatabaseConstant.py
@Time    : 2019/12/2 11:12
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""


class DatabaseTest:
    """测试环境数据库常量"""
    # 连接地址
    connect_url = "192.168.10.181"

    # 连接用户名
    username = "root"

    # 连接密码
    password = "yg123456"

    # 默认数据库
    lydd = "hy_hydd"
    lytc = "hy_lytc_parking_lot_manage"
    lysh = "hy_lysh_merchant_manage"
    xxfb = "hy_xxfb_information_platform"
    jtgl = "hy_lyjtgl"
    lyjc = "hy_lyjc_industrial_operation_monitoring"


class DatabaseProduction:
    """生产环境数据库常量"""
    # 连接地址
    connect_url = "192.168.10.210"

    # 连接用户名
    username = "root"

    # 连接密码
    password = "yg987654321"

    # 默认数据库
    lydd = "hy_hydd"
    lytc = "hy_lytc_parking_lot_manage"
    lysh = "hy_lysh_merchant_manage"
    xxfb = "hy_xxfb_information_platform"
    jtgl = "hy_lyjtgl"
    lyjc = "hy_lyjc_industrial_operation_monitoring"
