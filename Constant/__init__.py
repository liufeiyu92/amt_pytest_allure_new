"""
@File    : __init__.py.py
@Time    : 2019/12/2 11:12
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Settings import Config
from Constant.ApiConstant import ErrorApiNumber
from Constant.ConfigConstant import ConfigTitle


# =====================================================================
#                           接口-数据层                                 #
# =====================================================================

# 接口相应时间列表,单位毫秒
API_RESPONSE_TIME_LIST = []

# 接口执行结果列表
API_RESPONSE_LIST = []

# 洪雅交通管理平台-用户名字列表
API_USER_NAME_LIST = []

# 洪雅交通管理平台-单位ID列表
API_COMPANY_ID_LIST = []

# 洪雅交通管理平台-角色ID列表
API_ROLE_ID_LIST = []

# 洪雅交通管理平台-角色名称列表
API_ROLE_NAME_LIST = []

# 接口错误数据
API_ERROR_RESPONSE_DATA = []
