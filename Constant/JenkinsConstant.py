"""
@File    : JenkinsConstant.py
@Time    : 2019/12/2 15:20
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from Constant import *


class Jenkins:
    """Jenkins常量"""
    # 连接地址
    connect_url = "http://192.168.10.45:8080"

    # 连接用户名
    username = "admin"

    # 连接密码
    password = "admin"

    # api密码
    api_password = "119b80f0d125dad7cf5e07710b47e0c973"

    # allure报告地址
    _JENKINS_ALLURE_REPORT_URL = "allure_report_url"

    @property
    def allure_report_url(self):
        """
        allure报告地址
        :return:
        """
        para = (ConfigTitle.TITLE_JENKINS, self._JENKINS_ALLURE_REPORT_URL)
        return Config.Config.get_config(para)

    @allure_report_url.setter
    def allure_report_url(self, value: str):
        """
        设置allure报告地址
        """
        if not isinstance(value, str):
            raise Exception('请传入正确的url')
        else:
            Config.Config.set_config(ConfigTitle.TITLE_JENKINS, self._JENKINS_ALLURE_REPORT_URL, value)


if __name__ == '__main__':
    pass
