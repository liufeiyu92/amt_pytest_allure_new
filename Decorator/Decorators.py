"""
@File    : Decorators.py
@Time    : 2019/11/26 17:26
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from functools import wraps


def login(old):
    """装饰器"""

    @wraps(old)
    def inner(*args, **kwargs):
        print('**********')
        res = old(*args, **kwargs)
        print('**********')
        return res

    return inner
