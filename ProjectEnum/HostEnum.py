"""
@File    : HostEnum.py
@Time    : 2019/12/18 9:52
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum, unique


@unique
class HostMain(Enum):
    LYJT_HOST = "lyjt_host"
    LYSH_HOST = "lysh_host"
    XXFB_HOST = "xxfb_host"
    LYTC_HOST = "lytc_host"
    LYJC_HOST = "lyjc_host"
    LOGIN_HOST = "login_host"


if __name__ == '__main__':
    pass
