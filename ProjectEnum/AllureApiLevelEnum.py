"""
@File    : AllureApiLevelEnum.py
@Time    : 2019/12/4 9:09
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum, unique


@unique
class ApiImportantLevel(Enum):
    """
    allure报告接口重要性枚举
    blocker > critical > normal > minor > trivial
    """
    # 优先级-最低
    Lowest = "trivial"

    # 优先级-低
    Low = "minor"

    # 优先级-中
    Medium = "normal"

    # 优先级-高
    High = "critical"

    # 优先级-最高
    Highest = "blocker"


if __name__ == '__main__':
    pass
