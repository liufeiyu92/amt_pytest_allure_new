"""
@File    : JiraIssueEnum.py
@Time    : 2019/12/13 14:36
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum, unique


@unique
class IssueStatus(Enum):
    """问题单状态枚举类"""
    # 待办
    ToDo = "11"
    # 处理中
    InProgress = "21"
    # 完成
    Done = "31"
